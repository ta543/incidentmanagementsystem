import unittest
from detect import detect_anomalies

class TestAnomalyDetection(unittest.TestCase):
    def test_detect_anomalies(self):
        self.assertTrue(detect_anomalies({'event_type': 'unauthorized_access', 'user': 'JohnDoe'}))
        self.assertFalse(detect_anomalies({'event_type': 'normal_access', 'user': 'JaneDoe'}))

if __name__ == '__main__':
    unittest.main()

