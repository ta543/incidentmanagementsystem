# PowerShell script to configure system settings
Write-Host 'Starting system setup...'

# Example: Set timezone
tzutil /s 'UTC'

Write-Host 'System setup completed.'

