import logging

def detect_anomalies(data):
    if data['event_type'] == 'unauthorized_access':
        logging.warning('Unauthorized access detected')
        return True
    return False

if __name__ == '__main__':
    event_data = {'event_type': 'unauthorized_access', 'user': 'JohnDoe'}
    anomaly_detected = detect_anomalies(event_data)
    if anomaly_detected:
        print('Anomaly detected:', event_data)

