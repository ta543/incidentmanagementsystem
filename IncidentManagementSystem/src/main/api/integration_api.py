from flask import Flask, request, jsonify

app = Flask(__name__)

@app.route('/event', methods=['POST'])
def receive_event():
    event_data = request.json
    print('Received event:', event_data)
    return jsonify({'status': 'success', 'message': 'Event processed'}), 200

if __name__ == '__main__':
    app.run(debug=True, port=5000)

